import { CrudRepo } from '../../core/service/repo/CrudRepo';
import { SchemaNames } from '../../model/schema/Schema';
import { TodoItem } from '../../model/TodoItem';

/**
 * Repo for todo items.
 */
export class TodoItemRepo extends CrudRepo<TodoItem> {

  public TAB_NAME: any = SchemaNames.TODO_ITEM;

  /**
   * Retrieve a entity by the id of the parent todo.
   */
  public async findByTodoId(id: any): Promise<TodoItem[]> {
    var dataFromDb = await this.db.getAll(this.TAB_NAME);
    var entities: TodoItem[] = [];

    for (let data of dataFromDb) {
      if (data.todoId == id) {
        entities.push(this.toEntity(data));
      }
    }

    return entities;
  }

  protected toEntity(obj: any): TodoItem {
    var todo = new TodoItem(obj.task);
    todo.id = obj.id;
    todo.todoId = obj.todoId;
    todo.complete = obj.complete;
    todo.completedAt = obj.completedAt;
    todo.createdAt = obj.createdAt;
    todo.updatedAt = obj.updatedAt;
    return todo;
  }

  protected fromEntity(entity: TodoItem): {
    id: string;
    todoId: string;
    task: string;
    complete: boolean;
    completedAt: Date | null;
    createdAt: Date;
    updatedAt: Date;
  } {
    return {
      id: entity.id,
      todoId: entity.todoId,
      task: entity.task,
      complete: entity.complete,
      completedAt: entity.completedAt,
      createdAt: entity.createdAt,
      updatedAt: entity.updatedAt,
    };
  }
}

/**
 * Factory for the TodoItemRepo.
 */
export class TodoItemRepoFactory {
  private static instance: TodoItemRepo;

  public static async getInstance(): Promise<TodoItemRepo> {
    if (!TodoItemRepoFactory.instance) {
      TodoItemRepoFactory.instance = new TodoItemRepo();
      await TodoItemRepoFactory.instance.init();
    }
    return TodoItemRepoFactory.instance;
  }
}