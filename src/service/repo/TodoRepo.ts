import { Todo } from '../../model/Todo';
import { CrudRepo } from '../../core/service/repo/CrudRepo';
import { SchemaNames } from '../../model/schema/Schema';
import { Repeat } from '../../enum/Repeat';

/**
 * Repo for todos.
 */
export class TodoRepo extends CrudRepo<Todo> {

  public TAB_NAME: any = SchemaNames.TODO;

  protected toEntity(obj: any): Todo {
    var todo = new Todo(obj.task);
    todo.id = obj.id;
    todo.doAt = obj.doAt;
    todo.repeat = obj.repeat;
    todo.complete = obj.complete;
    todo.completedAt = obj.completedAt;
    todo.showCompleted = obj.showCompleted;
    todo.createdAt = obj.createdAt;
    todo.updatedAt = obj.updatedAt;
    return todo;
  }

  protected fromEntity(entity: Todo): {
    id: string;
    task: string;
    doAt: Date;
    repeat: Repeat;
    complete: boolean;
    completedAt: Date | null;
    showCompleted: boolean;
    createdAt: Date;
    updatedAt: Date;
  } {
    return {
      id: entity.id,
      task: entity.task,
      doAt: entity.doAt,
      repeat: entity.repeat,
      complete: entity.complete,
      completedAt: entity.completedAt,
      showCompleted: entity.showCompleted,
      createdAt: entity.createdAt,
      updatedAt: entity.updatedAt,
    };
  }
}

/**
 * Factory for the TodoRepo.
 */
export class TodoRepoFactory {
  private static instance: TodoRepo;

  public static async getInstance(): Promise<TodoRepo> {
    if (!TodoRepoFactory.instance) {
      TodoRepoFactory.instance = new TodoRepo();
      await TodoRepoFactory.instance.init();
    }
    return TodoRepoFactory.instance;
  }
}