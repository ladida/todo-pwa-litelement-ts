import { StateService } from "../../core/service/state/StateService";
import { TodoRepo, TodoRepoFactory } from "../repo/TodoRepo";
import { Todo } from "../../model/Todo";
import { IdGen } from "../../core/tool/IdGen";
import { TodoItem } from "../../model/TodoItem";
import { TodoItemRepo, TodoItemRepoFactory } from "../repo/TodoItemRepo";

/**
 * State service for the todolist
 * 
 * some day it will provide a
 *  - full CRUD+ api
 *  - sync with the repo
 *  - caching of data - if necessary
 */
export class TodoListStateService extends StateService {

  private todoRepo: TodoRepo;
  private todoItemRepo: TodoItemRepo;

  public async init() {
    this.todoRepo = await TodoRepoFactory.getInstance();
    this.todoItemRepo = await TodoItemRepoFactory.getInstance();
  }

  public async findAll(): Promise<Todo[]> {
    var todos = await this.todoRepo.findAll();

    for (var todo of todos) {
      await this.findAndAddItems(todo);
    }

    return todos as Todo[];
  }

  public async findById(id: string): Promise<Todo> {
    var todo = await this.todoRepo.findById(id);
    return await this.findAndAddItems(todo);
  }

  private async findAndAddItems(todo: Todo): Promise<Todo> {
    var items = await this.todoItemRepo.findByTodoId(todo.id);
    todo.addItems(items);
    return todo;
  }

  public async create(task: string) {
    var todo = new Todo(task);
    todo.id = IdGen.generate(10);

    await this.todoRepo.create(todo);
    this.informObservers();
  }

  public async update(todo: Todo) {
    await this.todoRepo.update(todo);
  }

  public async complete(todo: Todo) {
    todo.complete = true;
    todo.completedAt = new Date();
    await this.todoRepo.update(todo);
  }

  public async completeUndo(todo: Todo) {
    todo.complete = false;
    todo.completedAt = null;
    await this.todoRepo.update(todo);
  }

  public async delete(todo: Todo) {
    for (var item of todo.getItems()) {
      await this.todoItemRepo.delete(item);
    }
    await this.todoRepo.delete(todo);
    this.informObservers();
  }

  /**
   * Add a new TodoItem to a Todo
   * 
   * @param todo todo that will get a new item
   * @param task task for the new item
   */
  public async addItem(todo: Todo, task: string) {
    var todoItem = new TodoItem(task);
    todoItem.id = IdGen.generate(10);
    todoItem.todoId = todo.id;
    todo.addItem(todoItem);
    await this.todoItemRepo.create(todoItem);
  }

  public async completeItem(todoItem: TodoItem) {
    todoItem.complete = true;
    todoItem.completedAt = new Date();
    await this.todoItemRepo.update(todoItem);
  }

  public async completeUndoItem(todoItem: TodoItem) {
    todoItem.complete = false;
    todoItem.completedAt = null;
    await this.todoItemRepo.update(todoItem);
  }
}

/**
 * Factory for the TodoListStateService.
 */
export class TodoListStateServiceFactory {
  private static instance: TodoListStateService;

  public static async getInstance(): Promise<TodoListStateService> {
    if (!TodoListStateServiceFactory.instance) {
      TodoListStateServiceFactory.instance = new TodoListStateService();
      await TodoListStateServiceFactory.instance.init();
    }
    return TodoListStateServiceFactory.instance;
  }
}