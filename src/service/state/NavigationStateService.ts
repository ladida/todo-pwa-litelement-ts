import { StateService } from "../../core/service/state/StateService";
import { MainViews } from "../../enum/MainViews";

/**
 * State service for navigation.
 */
export class NavigationStateService extends StateService {

  /**
   * the view
   * start at the todolist view
   */
  private actualView: MainViews = MainViews.TODOLISTS;

  public async init() {
  }

  public getActualView() {
    return this.actualView;
  }

  public setTodosListView() {
    this.actualView = MainViews.TODOLISTS;
    this.informObservers();
  }

  public setStatisticsView() {
    this.actualView = MainViews.STATISTICS;
    this.informObservers();
  }

  public setAboutView() {
    this.actualView = MainViews.ABOUT;
    this.informObservers();
  }
}

/**
 * Factory for the NavigationStateService.
 */
export class NavigationStateServiceFactory {
  private static instance: NavigationStateService;

  public static async getInstance(): Promise<NavigationStateService> {
    if (!NavigationStateServiceFactory.instance) {
      NavigationStateServiceFactory.instance = new NavigationStateService();
      await NavigationStateServiceFactory.instance.init();
    }
    return NavigationStateServiceFactory.instance;
  }
}