import { BaseScheduler } from "../../core/service/scheduler/BaseScheduler";

export class ServiceWorkerUpdateCheckScheduler extends BaseScheduler {

  private reg: ServiceWorkerRegistration;

  public async init() {
    this.delayTime = 120000;
  }

  public setRegistration(reg: ServiceWorkerRegistration) {
    this.reg = reg;
  }

  protected isRunnable(): boolean {
    var isRunnable = super.isRunnable();
    if (!this.reg) {
      isRunnable = false;
      console.error('ServiceWorkerRegistration undefined');
    }
    return isRunnable;
  }

  protected async executeTask() {
    if (this.reg) {
      this.reg.update();
    } else {
      console.error('ServiceWorkerRegistration undefined');
    }
  }
}


/**
 * Factory for the ServiceWorkerUpdateCheckScheduler.
 */
export class ServiceWorkerUpdateCheckSchedulerFactory {
  private static instance: ServiceWorkerUpdateCheckScheduler;

  public static async getInstance(): Promise<ServiceWorkerUpdateCheckScheduler> {
    if (!ServiceWorkerUpdateCheckSchedulerFactory.instance) {
      ServiceWorkerUpdateCheckSchedulerFactory.instance = new ServiceWorkerUpdateCheckScheduler();
      await ServiceWorkerUpdateCheckSchedulerFactory.instance.init();
    }
    return ServiceWorkerUpdateCheckSchedulerFactory.instance;
  }
}