import { html, css, customElement, property } from 'lit-element';
import { BaseComp } from '../../../core/gui/BaseComp';

@customElement('accordion-comp')
export class AccordionComp extends BaseComp {

  @property()
  private detailsOpen = false;

  @property()
  private toggleValue = ToggleValue.OPEN;

  render() {
    return html`
      <div class="card"> 
        <div class="header card">
          <div class="content-header">
            <slot name="header"></slot>
          </div>
          <div class="detail-toggler" @click="${this.detailToggle}">
            <div class="toggle-char">${this.toggleValue}</div>
          </div>
        </div>

        <div id="body" class="body">
          <slot name="body"></slot>
        </div>
      </div>
    `;
  }

  /**
   * Switch the toggle state and value.
   */
  private detailToggle() {
    this.detailsOpen = !this.detailsOpen;

    if (this.detailsOpen) {
      this.toggleValue = ToggleValue.CLOSE;
      this.openDetails();
    } else {
      this.toggleValue = ToggleValue.OPEN;
      this.closeDetails();
    }
  }

  private openDetails() {
    var body = this.getElementById("body");
    if (body) {
      body.classList.add('active');
    }
  }

  private closeDetails() {
    var body = this.getElementById("body");
    if (body) {
      body.classList.remove('active');
    }
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .card {
        border-radius: 16px;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
      }
      .header {
        display: flex;
      }
      .content-header {
        width: calc(100% - 60px);
        padding: 16px;
      }
      .detail-toggler {
        width: 60px;
        border-radius: 0px 16px 16px 0px;
        display: flex;
        justify-content: center; /* align horizontal */
        align-items: center; /* align vertical */
        cursor: pointer;
      }
      .detail-toggler:hover {
        background-color: lightgrey;
      }
      .toggle-char {
        user-select: none;
        font-size: 24px;
      }

      .active {
        display: block !important;
      }
      .body {
        padding: 16px;
        display: none;
      }
    `;
  }
}

class ToggleValue {
  public static OPEN = '+';
  public static CLOSE = '-';
}