import { html, css, customElement, property } from 'lit-element';
import './accordion/accordion-comp';
import './../components/button/red-button-comp';
import './../components/button/blue-button-comp';
import './../components/textfield/textfield-comp';
import './../components/checkbox/checkbox-comp';
import { Todo } from '../../model/Todo';
import { TodoListStateService, TodoListStateServiceFactory } from '../../service/state/TodoListStateService';
import { IStateObserver } from '../../core/service/state/IStateObserver';
import { TodoItem } from '../../model/TodoItem';
import { ConfirmDialog } from './dialog/confirm-dialog';
import { BaseComp } from '../../core/gui/BaseComp';
import { TextfieldComp } from './../components/textfield/textfield-comp';

@customElement('todo-comp')
export class TodoComp extends BaseComp implements IStateObserver {
  @property()
  private todo: Todo;

  private deleteTodoConfirmDialog: ConfirmDialog;

  private todolistService: TodoListStateService;

  constructor(todo: Todo) {
    super();
    this.todo = todo;
  }

  protected async initialize() {
    super.initialize();

    this.todolistService = await TodoListStateServiceFactory.getInstance();
    this.todolistService.registerObserver(this);
  }

  public async stateChanged() {
  }

  render() {
    return html`
      <accordion-comp style="margin-top: 16px; margin-bottom: 16px">
        <div slot="header">
          <div style="display: flex;">
            <checkbox-comp ?checked="${this.todo.complete}" @change="${this.updateTodoStatus}" style="margin-right: 8px;"></checkbox-comp>
            <label style="font-size: 16px;">${this.todo.task}</label>
          </div>
        </div>
        <div slot="body" class="body">
          <div class="todo-items">
            <div style="display: flex;">
              <div style="width: 70%; box-sizing: border-box; padding-right: 8px;" @keyup="${this.shortcutListener}">                  
                <textfield-comp id="taskTextfield" placeholder="Task"></textfield-comp>
              </div>
              <blue-button-comp style="width: calc(30% - 8px);" @click="${this.addTodoItem}" title="Add"></blue-button-comp>
            </div>
            <div style="padding: 12px">
              ${this.todo.getItems().map(todoItem => ((this.todo.showCompleted && todoItem.complete) || todoItem.complete == false) ? html`
                <div style="display: flex; padding: 8px 0px;">
                  <checkbox-comp ?checked="${todoItem.complete}" @change="${(e: { target: HTMLInputElement }) => this.updateTodoItemStatus(e, todoItem)}" style="margin-top: -1px; margin-right: 8px;"></checkbox-comp>
                  <label> ${todoItem.task}</label>
                </div>
              ` : '')}
            </div>
          </div>
          <div class="info">
            <table>
              <tr>
                <td class="td-name">show completed:</td>
                <td class="td-value">
                  <checkbox-comp ?checked="${this.todo.showCompleted}" @change="${this.updateShowCompleted}" style="margin-top: 1px; margin-right: 8px;"></checkbox-comp>
                </td>
              </tr>
              <tr><td class="td-name">do at:</td><td class="td-value">${this.todo.doAt ? this.todo.doAt.toLocaleString() : ''}</td></tr>
              <tr><td class="td-name">repeat:</td><td class="td-value">${this.todo.repeat}</td></tr>
              <tr><td class="td-name">created at:</td><td class="td-value">${this.todo.createdAt ? this.todo.createdAt.toLocaleString() : ''}</td></tr>
              <tr><td class="td-name">completed at:</td><td class="td-value">${this.todo.completedAt ? this.todo.completedAt.toLocaleString() : ''}</td></tr>
              <tr><td class="td-name">updated at:</td><td class="td-value">${this.todo.updatedAt ? this.todo.updatedAt.toLocaleString() : ''}</td></tr>
            </table>
            
            <red-button-comp @click="${this.openDeleteDialog}" style="width: 150px; margin: 10px auto;" title="Delete"></red-button-comp>
            <confirm-dialog id="deleteDialog" headline="Delete Todo" text="Do you want to delete the todo '${this.todo.task}'?" @clickYes="${this.deleteTodo}}"></confirm-dialog>
          </div>
        </div>
      </accordion-comp>
    `;
  }


  static get styles() {
    return css`
      :host {
        display: block;
      }
      .body {
        display: flex;
      }
      .todo-items {
        width: 60%;
      }
      .info {
        min-width: max-content;
        padding: 10px;
      }
      .td-name {
        text-align: right;
        padding-right: 8px;
      }
      .td-value {
        text-align: left;
      }
      @media only screen and (max-width: 800px) {
        .body {
          display: block;
        }
        .todo-items {
          width: 100%;
          border-bottom: 1px solid grey;
        }
        .info {
          width: max-content;
          margin: auto;
        }
      }
    `;
  }

  private shortcutListener(e: KeyboardEvent) {
    if (e.key === 'Enter') {
      this.addTodoItem();
    }
  }

  private async addTodoItem() {
    var textfieldComp = this.getElementById('taskTextfield') as TextfieldComp;
    if (textfieldComp) {
      if (textfieldComp.value) {
        await this.todolistService.addItem(this.todo, textfieldComp.value);
        textfieldComp.value = '';
        this.requestUpdate();
      }
    }
  }

  private async updateTodoStatus(e: { target: HTMLInputElement }) {
    if (e.target.checked) {
      await this.todolistService.complete(this.todo);
    } else {
      await this.todolistService.completeUndo(this.todo);
    }

    this.todo = await this.todolistService.findById(this.todo.id);
  }

  private async updateTodoItemStatus(e: { target: HTMLInputElement }, todoItem: TodoItem) {
    if (e.target.checked) {
      await this.todolistService.completeItem(todoItem);
    } else {
      await this.todolistService.completeUndoItem(todoItem);
    }

    this.todo = await this.todolistService.findById(this.todo.id);
  }

  private openDeleteDialog() {
    this.deleteTodoConfirmDialog = this.getElementById('deleteDialog') as ConfirmDialog;

    if (this.deleteTodoConfirmDialog) {
      this.deleteTodoConfirmDialog.open();
    }
  }

  private deleteTodo() {
    this.todolistService.delete(this.todo);

    if (this.deleteTodoConfirmDialog) {
      this.deleteTodoConfirmDialog.close();
    }
  }

  private async updateShowCompleted(e: { target: HTMLInputElement }) {
    this.todo.showCompleted = e.target.checked;
    await this.todolistService.update(this.todo);
    this.todo = await this.todolistService.findById(this.todo.id);
  }
}

