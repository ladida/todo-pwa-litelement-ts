import { html, customElement, property } from 'lit-element';
import { WithoutShadowDomBaseComp } from '../../core/gui/WithoutShadowDomBaseComp';
import { ServiceWorkerInstallationStateServiceFactory, ServiceWorkerInstallationStateService, ServiceWorkerInstallationStateType } from '../../core/service/state/ServiceWorkerInstallationStateService';
import { IStateObserver } from '../../core/service/state/IStateObserver';

@customElement('serviceworker-installation-view')
class ServiceWorkerInstallationView extends WithoutShadowDomBaseComp implements IStateObserver {

  @property()
  private state: string;

  private serviceWorkerInstallationStateService: ServiceWorkerInstallationStateService;

  constructor() {
    super();
  }

  protected async initialize() {
    super.initialize();
    this.serviceWorkerInstallationStateService = await ServiceWorkerInstallationStateServiceFactory.getInstance();
    this.serviceWorkerInstallationStateService.registerObserver(this);
  }

  async stateChanged() {
    this.state = this.serviceWorkerInstallationStateService.getState();
  }

  render() {
    return html`
      <div class="w3-bottom w3-dark-grey">
        ${this.state == ServiceWorkerInstallationStateType.INSTALLED ? html`${this.intalledTemplate}` : html``}
        ${this.state == ServiceWorkerInstallationStateType.ACTIVATING ? html`${this.activatingTemplate}` : html``}
      </div>
    `;
  }

  get intalledTemplate() {
    return html`
      <style>
        .btns {
          display: flex;
        }
        .btn {
          width: 70px;
        }
        @media only screen and (max-width: 500px) {
          .btns {
            flex-direction: column;
          }
        }
      </style>
      <div class="app-size" style="margin: auto;">
        <div style="display: flex; float: right; margin: 16px">
          <div>
            A new version is available. Activate and restart to get the latest features.
          </div>
          <div class="btns">
            <button class="w3-btn w3-round-xlarge w3-ripple w3-light-grey btn" style="margin-left: 8px; margin-bottom: 8px;" @click="${this.restart}">ok</button>
            <button class="w3-btn w3-round-xlarge w3-ripple w3-light-grey btn" style="margin-left: 8px; margin-bottom: 8px;" @click="${this.later}">later</button>
          </div>
        </div>
      </div>
    `;
  }

  get activatingTemplate() {
    return html`
      <div style="width: 100vw; height: 100vh; z-index: 100;">
        <div style="width: max-content; margin: auto; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
          activating and reload
        </div>
      </div>
    `;
  }

  private restart() {
    this.serviceWorkerInstallationStateService.activate();
  }

  private later() {
    this.state = "";
  }
}