import { html, customElement } from 'lit-element';
import { WithoutShadowDomBaseComp } from '../../core/gui/WithoutShadowDomBaseComp';

@customElement('statistics-view')
class StatisticsView extends WithoutShadowDomBaseComp {

  render() {
    return html`
      Statistics...
    `;
  }
}