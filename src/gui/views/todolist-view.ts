import { html, css, customElement, property } from 'lit-element';
import { Todo } from '../../model/Todo';
import { VisibilityFilters } from '../../enum/VisibilityFilters';
import { TodoComp } from '../components/todo-comp';
import './../components/dialog/confirm-dialog';
import './../components/button/blue-button-comp';
import { TextfieldComp } from './../components/textfield/textfield-comp';
import { TodoListStateService, TodoListStateServiceFactory } from '../../service/state/TodoListStateService';
import { IStateObserver } from '../../core/service/state/IStateObserver';
import { BaseComp } from '../../core/gui/BaseComp';

@customElement('todolist-view')
export class TodoListView extends BaseComp implements IStateObserver {
  @property()
  private todos: Todo[];
  @property()
  private filter: string;

  private todolistService: TodoListStateService;
  private todoComps: Map<string, TodoComp>;

  constructor() {
    super();
    this.todos = [];
    this.todoComps = new Map<string, TodoComp>();
    this.filter = VisibilityFilters.SHOW_ALL;
  }

  async initialize() {
    super.initialize();

    this.todolistService = await TodoListStateServiceFactory.getInstance();
    this.todolistService.registerObserver(this);
    this.stateChanged();
  }

  async stateChanged() {
    this.todos = await this.todolistService.findAll();
    var newTodoComps = new Map<string, TodoComp>();

    this.todos.map(
      todo => {
        var todoComp = this.todoComps.get(todo.id);
        if (todoComp) {
          newTodoComps.set(todo.id, todoComp);
        } else {
          newTodoComps.set(todo.id, new TodoComp(todo));
        }
      }
    );
    this.todoComps = newTodoComps;
  }

  render() {
    return html`
      <div class="container">         
        <div class="input-container">
          <textfield-comp id="taskTextfield" placeholder="Task" class="textfield" @keyup="${this.shortcutListener}"></textfield-comp>
          <blue-button-comp title="Add" class="button" @click="${this.addTodo}"></blue-button-comp>
        </div>
        <div class="todolist-container">
          ${this.todoComps.values()}
        </div>
      </div>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        padding: 16px;
      }
      .input-container {
        display: flex;
      }
      .textfield {
        width: 66.6%;
      }
      .button {
        width: 33.3%;
        margin-left: 8px;
      }
    `;
  }

  shortcutListener(e: KeyboardEvent) {
    if (e.key === 'Enter') {
      this.addTodo();
    }
  }

  async addTodo() {
    var textfieldComp = this.getElementById('taskTextfield') as TextfieldComp;
    if (textfieldComp) {
      if (textfieldComp.value) {
        this.todolistService.create(textfieldComp.value);
        textfieldComp.value = '';
      }
    }
  }
}

