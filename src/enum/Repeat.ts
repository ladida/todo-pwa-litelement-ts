/**
 * Represent the repeat types for a todo.
 */
export class Repeat {
  public static ONCE: string = 'ONCE';
  public static DAILY: string = 'DAILY';
  public static WEEKLY: string = 'WEEKLY';
  public static MONTHLY: string = 'MONTHLY';
}