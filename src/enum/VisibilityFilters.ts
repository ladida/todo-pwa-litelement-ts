export class VisibilityFilters {
  static SHOW_ALL: string;
  constructor(
    public SHOW_ALL: string = 'All',
    public SHOW_ACTIVE: string = 'Active',
    public SHOW_COMPLETED: string = 'Completed',
  ) { }
}
