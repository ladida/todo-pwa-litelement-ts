/**
 * Represent the navigation targets for the main view
 */
export class MainViews {
  public static TODOLISTS: string = 'TODOLISTS';
  public static STATISTICS: string = 'STATISTICS';
  public static ABOUT: string = 'ABOUT';
}
