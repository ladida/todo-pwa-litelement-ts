import { BaseService } from "../BaseService"

/**
 * the base for schedulers. they are based on a delay.
 */
export abstract class BaseScheduler extends BaseService {

  private isRunning = false;
  protected delayTime: number;

  /**
   * method to check the properties. should return true if all is fine, else false.
   */
  protected isRunnable() {
    var isRunnable = true;
    if (!this.delayTime) {
      isRunnable = false;
      console.error('delay undefined');
    }
    return isRunnable;
  }

  /**
   * the task they is to execute.
   */
  protected abstract executeTask(): void;

  /**
   * start the scheduler if they is runnable.
   */
  public async start() {
    if (this.isRunnable()) {
      this.isRunning = true;
      this.run();
    }
  }

  /**
   * stops the scheduler.
   */
  public stop() {
    //console.log("[ServiceWorkerUpdateCheckerScheduler] stop");
    this.isRunning = false;
  }

  /**
   * run the task every [delay] milliseconds.
   */
  private async run() {
    while (this.isRunning) {
      await this.delay(this.delayTime);
      if (this.isRunning) {
        await this.executeTask();
      }
    }
  }

  /**
   * a method the set the scheduler to sleep.
   * 
   * @param ms milliseconds
   */
  private async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}