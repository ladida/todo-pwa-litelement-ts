import { openDB, IDBPDatabase } from 'idb';
import { Schema, SchemaNames } from '../../../../model/schema/Schema';

/**
 * Handles the connection to the indexeddb
 */
export class IndexedDbConnection {

  private db: IDBPDatabase<Schema>;


  public async getOpenedDb(): Promise<IDBPDatabase<Schema>> {
    if (!this.db) {
      await this.openDb();
    }
    return this.db;
  }

  /**
    * Opens the db and create/update schemas.
    */
  private async openDb() {
    if (!window.indexedDB) {
      window.alert("Ihr Browser unterstützt keine stabile Version von IndexedDB. Dieses und jenes Feature wird Ihnen nicht zur Verfügung stehen.");
    }

    this.db = await openDB<Schema>(SchemaNames.DB_NAME, 5, {
      upgrade(db, oldVersion, newVersion) {
        /**
         * to 5 - restart with the version
         */
        if (newVersion == 5) {
          if (db.objectStoreNames.contains(SchemaNames.TODO)) {
            db.deleteObjectStore(SchemaNames.TODO);
          }
          if (db.objectStoreNames.contains(SchemaNames.TODO_ITEM)) {
            db.deleteObjectStore(SchemaNames.TODO_ITEM);
          }

          db.createObjectStore(SchemaNames.TODO);
          db.createObjectStore(SchemaNames.TODO_ITEM);
        }
      },
    });
  }
}

/**
 * Factory for the IndexedDbConnection.
 */
export class IndexedDbConnectionFactory {
  private static instance: IndexedDbConnection;

  public static async getInstance(): Promise<IndexedDbConnection> {
    if (!IndexedDbConnectionFactory.instance) {
      IndexedDbConnectionFactory.instance = new IndexedDbConnection();
    }
    return IndexedDbConnectionFactory.instance;
  }
}