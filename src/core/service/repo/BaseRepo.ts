import { IDBPDatabase } from 'idb';
import { Schema } from '../../../model/schema/Schema';
import { BaseService } from '../BaseService';
import { Entity } from '../../model/Entity';
import { IndexedDbConnection, IndexedDbConnectionFactory } from './indexeddb/IndexedDbConnection';

/**
 * Base Repo.
 * 
 * Create the db and schema.
 */
export abstract class BaseRepo<T extends Entity> extends BaseService {

  protected db: IDBPDatabase<Schema>;

  public async init() {
    var indexedDbConnection: IndexedDbConnection = await IndexedDbConnectionFactory.getInstance();
    this.db = await indexedDbConnection.getOpenedDb();
  }

  /**
   * Convert the db object to an entity.
   * 
   * @param obj the object from db
   */
  protected abstract toEntity(obj: any): T;

  /**
   * Convert the entity to a db object.
   * 
   * @param entity the entity
   */
  protected abstract fromEntity(entity: T): any;
}