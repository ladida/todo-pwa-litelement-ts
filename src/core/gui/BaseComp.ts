import { LitElement } from 'lit-element';


/**
 * Base component.
 */
export abstract class BaseComp extends LitElement {

  /**
   * Get the element by id.
   * 
   * @param id id
   */
  protected getElementById(id: string) {
    if (this.shadowRoot) {
      return this.shadowRoot.getElementById(id);
    }
    return document.getElementById(id);
  }
}
