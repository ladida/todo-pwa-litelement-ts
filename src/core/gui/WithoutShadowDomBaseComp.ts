import { BaseComp } from "./BaseComp";

/**
 * Base component without shadow dom.
 */
export abstract class WithoutShadowDomBaseComp extends BaseComp {
  
  /**
   * no shadow dom
   */
  protected createRenderRoot() {
    return this;
  }
}
