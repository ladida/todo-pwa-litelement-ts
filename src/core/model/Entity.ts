export abstract class Entity {
  public id: string;
  public createdAt: Date;
  public updatedAt: Date;

  constructor() {
    this.createdAt = new Date();
  }

  public update() {
    this.updatedAt = new Date();
  }
}