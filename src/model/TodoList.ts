import { Entity } from "../core/model/Entity";
import { Todo } from "./Todo";

export class TodoList extends Entity {

  public title: string;
  private todos: Todo[] = [];

  constructor(title: string) {
    super();
    this.title = title;
  }

  public addTodo(todo: Todo) {
    this.todos.push(todo);
  }

  public removeTodo(todo: Todo) {
    this.todos = this.todos.filter(itm => itm != todo);
  }

  public geTodos(): Todo[] {
    return this.todos;
  }
}