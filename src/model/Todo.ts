import { TodoItem } from './TodoItem';
import { Entity } from '../core/model/Entity';
import { Repeat } from '../enum/Repeat';

export class Todo extends Entity {

  public task: string;
  public doAt: Date;
  public repeat: Repeat = Repeat.ONCE;
  public complete: boolean = false;
  public completedAt: Date | null;
  public showCompleted: boolean = false;
  private items: TodoItem[] = [];

  constructor(task: string) {
    super();
    this.task = task;
  }

  public addItem(item: TodoItem) {
    this.items.push(item);
  }

  public addItems(items: TodoItem[]) {
    for (var item of items) {
      this.addItem(item);
    }
  }

  public removeItem(item: TodoItem) {
    this.items = this.items.filter(itm => itm != item);
  }

  public getItems(): TodoItem[] {
    return this.items;
  }
}