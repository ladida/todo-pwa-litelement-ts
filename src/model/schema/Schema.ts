import { DBSchema } from 'idb';
import { Repeat } from '../../enum/Repeat';

export class SchemaNames {

  public static DB_NAME: string = 'my-db';

  public static TODO_LIST: any = 'todo-list';
  public static TODO: any = 'todo';
  public static TODO_ITEM: any = 'todo-item';
}

/**
 * Schema für Indexeddb (idb)
 */
export interface Schema extends DBSchema {
  'todo-list': {
    key: string;
    value: {
      id: string,
      title: string,
      createdAt: Date,
      updatedAt: Date,
    };
  };
  'todo': {
    key: string;
    value: {
      id: string,
      task: string,
      doAt: Date,
      repeat: Repeat,
      complete: boolean,
      completedAt: Date,
      showCompleted: boolean,
      createdAt: Date,
      updatedAt: Date,
    };
  };
  'todo-item': {
    key: string;
    value: {
      id: string,
      todoId: string,
      task: string,
      complete: boolean,
      completedAt: Date,
      createdAt: Date,
      updatedAt: Date,
    };
  };
}