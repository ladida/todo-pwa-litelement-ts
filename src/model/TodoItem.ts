import { Entity } from "../core/model/Entity";

export class TodoItem extends Entity {

  public todoId: string;
  public task: string;
  public complete: boolean = false;
  public completedAt: Date | null;

  constructor(task: string) {
    super();
    this.task = task;
  }
}