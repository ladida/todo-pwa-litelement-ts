import './css/index.css';
import './gui/views/header-view.ts';
import './gui/views/main-view.ts';
import './gui/views/serviceworker-installation-view.ts';
import './gui/views/todolist-view.ts';
import './gui/views/statistics-view.ts';
import './gui/views/about-view.ts';
import { ServiceWorkerInstallationStateService, ServiceWorkerInstallationStateServiceFactory } from './core/service/state/ServiceWorkerInstallationStateService';
import { ServiceWorkerUpdateCheckSchedulerFactory } from './service/scheduler/ServiceWorkerUpdateCheckScheduler';

window.addEventListener('load', () => {
  console.log('---------------------------[index.ts] register SW---------------------------');
  registerSW();
});

async function registerSW() {
  if ('serviceWorker' in navigator) {
    try {
      await navigator.serviceWorker.register('./service-worker.js').then(async reg => {
        var serviceWorkerInstallationStateService: ServiceWorkerInstallationStateService = await ServiceWorkerInstallationStateServiceFactory.getInstance();

        var scheduler = await ServiceWorkerUpdateCheckSchedulerFactory.getInstance();
        scheduler.setRegistration(reg);
        scheduler.start();

        /**
         * the browser is installing a new service worker
         */
        reg.addEventListener('updatefound', () => {
          const newWorker = reg.installing;
          if (newWorker) {
            serviceWorkerInstallationStateService.installingHandler(newWorker);

            newWorker.addEventListener('statechange', (event) => {
              if (event.currentTarget instanceof ServiceWorker) {
                let sw: ServiceWorker = event.currentTarget;

                if ("installed" == sw.state) {
                  serviceWorkerInstallationStateService.installedHandler();
                } else if ("activating" == sw.state) {
                  serviceWorkerInstallationStateService.activatingHandler();
                } else if ("activated" == sw.state) {
                  serviceWorkerInstallationStateService.activatedHandler();
                } else {
                  serviceWorkerInstallationStateService.unknownStateHandler();
                }
              }
            });
          }
        });
      });

      /**
       * fires when service worker is changed
       */
      navigator.serviceWorker.addEventListener('controllerchange', event => {
        console.log("[index.ts - controllerchange]");
        //console.log(event);
      });

      //console.log('[index.ts] ServiceWorker registration successfully.');
    } catch (e) {
      console.error('[index.ts] ServiceWorker registration failed. Sorry about that.', e);
    }
  } else {
    console.log('[index.ts] Your browser does not support ServiceWorker.');
  }
}