var VERSION = 1;

if ('workbox' in self) {
  workbox.precaching.precacheAndRoute(self.__precacheManifest || []);
  //console.log('[service-worker.js ' + VERSION + '] Caching erfolgreich');
}


self.addEventListener('install', function (pEvent) {
  //console.log('[service-worker.js ' + VERSION + '] Installation wird gestartet');
  //console.log(self);
  //console.log('[service-worker.js ' + VERSION + '] Installation ende');
});


self.addEventListener('activate', function (pEvent) {
  console.log('[service-worker.js ' + VERSION + '] ist aktiv');
});

self.addEventListener("message", (evt) => {
  if ("SKIP_WAITING" == evt.data) {
    //console.log('[service-worker.js ' + VERSION + '] SKIP_WAITING');
    self.skipWaiting();
  } else if ("CLAIM" == evt.data) {
    //console.log('[service-worker.js ' + VERSION + '] CLAIM');
    self.clients.claim();
    //console.log(self.clients);
  }
});