# todo-pwa-litelement-ts

A todo app as PWA. Written with TypeScript and Lit-Element.

# Accessible under

https://ladida.gitlab.io/todo-pwa-litelement-ts


# Installation

```sh
npm i
```

# Run

dev:
```sh
npm run dev
```

dev with service worker enabled:
```sh
npm run dev:sw
```

Prod:
```sh
npm run prod
```
